Source: libjdom2-intellij-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Andrej Shadura <andrewsh@debian.org>,
 Emmanuel Bourg <ebourg@apache.org>,
 Saif Abdul Cassim <saif.15@cse.mrt.ac.lk>
Build-Depends:
 debhelper-compat (= 13),
 gradle-debian-helper,
 default-jdk
Build-Depends-Indep:
 junit4,
 libisorelax-java,
 libjaxen-java,
 libjs-jquery,
 libjs-jquery-ui,
 libxalan2-java,
 libxerces2-java,
 maven-repo-helper (>= 1.5)
Standards-Version: 4.6.1
Vcs-Git: https://salsa.debian.org/java-team/libjdom2-intellij-java.git
Vcs-Browser: https://salsa.debian.org/java-team/libjdom2-intellij-java
Homepage: https://github.com/JetBrains/intellij-deps-jdom

Package: libjdom2-intellij-java
Architecture: all
Depends:
 libjaxen-java,
 ${misc:Depends}
Description: lightweight and fast XML DOM library (IntelliJ version)
 JDOM is, quite simply, a Java representation of an XML document. JDOM
 provides a way to represent that document for easy and efficient
 reading, manipulation, and writing. It has a straightforward API, is
 lightweight and fast, and is optimized for the Java programmer. It's
 an alternative to DOM and SAX, although it integrates well with both
 DOM and SAX.
 .
 This is a patched versions of JDOM project which is used in IntelliJ
 Platform. It is based on the JDOM 2.0.x branch and patched to restore
 compatibility with JDOM 1.1.
